PATH := ./node_modules/.bin:${PATH}

ifeq ($(OS),Windows_NT)
	COPY_CMD=robocopy /MIR
	RM_CMD=rd -r
else
	COPY_CMD=cp -r
	RM_CMD=rm -rf
endif

init:
	npm install

clean:
	-$(RM_CMD) lib

build: clean
	-$(COPY_CMD) src lib
	coffee-build -v 1.11.x -o lib src

dist: clean init build

publish: dist
	npm publish
